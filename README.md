# My Git Knowledge Base  ![GitLab Logo](gitlab-logo.jpg)

## Introduction
Learning Git can be an overwhelming process for newbies. This document provides reference information that you can use with your GitLab projects. 

## Prerequisites
* A Command-Line Interface (CLI) or VS Code.
* Create a GitLab account.
* Create a new repository.

## Typical Procedure
### Step 1: Fork the Repository
After you find a repository that you would like to work with, you will need to create a copy of the repository in your Gitlab repository area.

1. Click on the **Fork** button in the repository of interest
2. Complete the form
3. Click on **Fork project**

You will now have a copy of the repository of interest in your Gitlab repository.

## Step 2: Clone the Repository
You will now need to copy the repository in your Gitlab account to your computer.
 
1. Click on **Clone** button in your repository
2. Copy the URL for **SSH** to the clipboard
3. In the CLI, navigate to the folder where you would like the cloned repository to reside
4. Enter `<git clone {URL ADDRESS YOU JUST COPIED}>`

## Step 3: Set Up Credentials
You will need to tell Gitlab your name and email address because you are using SSH instead of HTTP.

1. In the CLI, Enter `<git config user.name {'YOUR NAME'}>`
2. Enter `<git config user.name>` to verify
3. Enter `<git config user.email {'YOUR EMAIL'}>`
4. Enter git config user.email to verify

## Step 4: Configure the Fork
You will need to sink the upstream repository with the repository in your Gitlab account.

1. Enter `<git remote -v>` to show the URLs of all remote repositories
2. Enter `<git remote add {UPSTREAM-REPOSITORY}>` (replace UPSTREAM-REPOSITORY) with the SSH URL from Step 2
3. Enter `<git remote -v>`

## Step 5: Check Out a New Branch

1. Enter `<git branch>` to see what branches exist (Option)
2. Enter `<git pull upstream main>` to pull down any updates from the upstream repository.
3. Enter `<git push origin main>` to push updates to your fork.
4. To create and check out a the new branch, Enter `<git checkout -b {branch-name}>` (replace branch-name with a name with no spaces

**Note: Leave off `<-b>` to check out an existing branch.**  

5. Enter `<git status>` the check the status (Option)
6. Enter `<git branch>` to see what branches exist (Option)

## Step 7: Edit File
Edit the markdown file README.md in VS Code

## Step 8: Stage, Commit and Push the Files 
After you have finished editing the files, you will need to update the repository in your Gitlab account.

1. Enter `<git status>` to see the status of all files in the repository of interest on your computer
2. Enter `<git add .>` to stage all files
3. Enter `<git status>` to see that all files have been staged
4. Enter `<git commit -m {'MESSAGE'}>` where MESSAGE is a description of what changed in lower case (i.e., updated file)
5. Enter `<git push --set-upstream origin {branch-name}>` to push the files up to your repository branch

## Step 9: Merge Request
To make a merge request, Click on the Merge button on the **main** page in your repository of interest. Do not use the Merge button on the Issues page.

## Helpful Links and Resources

[GitHub in Pictures](https://www.showwcase.com/show/18785/git-in-pictures)

[First Contributions to GitHub](https://github.com/firstcontributions/first-contributions)https://github.com/firstcontributions/first-contributions
